//
//  Results.swift
//  MoviesApp
//
//  Created by Gilang Sinawang on 19/04/20.
//  Copyright © 2020 Gilang Sinawang. All rights reserved.
//

import Foundation

struct Results: Codable{
    let results: [Movie]
    let page: Int
    let totalPages: Int
    let totalResults: Int
}
