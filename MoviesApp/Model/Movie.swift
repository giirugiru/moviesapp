//
//  Movie.swift
//  MoviesApp
//
//  Created by Gilang Sinawang on 19/04/20.
//  Copyright © 2020 Gilang Sinawang. All rights reserved.
//

import Foundation

struct Movie: Codable{
    let id: Int
    let title: String
    let ovrview: String
    let posterPath: String
    let backgroundPath: String
    let voteCount: Int
    let voteAverage: Double
    let releaseDate: String
}
